<!--
SPDX-FileCopyrightText: Yoran Heling <projects@yorhel.nl>
SPDX-License-Identifier: AGPL-3.0-only
-->

# SQLBin

SQLBin is a simple, fast, lightweight, efficient, AGPL-licensed, single-binary,
boringly-named and equally plain-looking multi-user web interface for running
and sharing queries against a PostgreSQL database, written in Crystal.

See the [SQLBin homepage](https://dev.yorhel.nl/sqlbin) for more information.

A live demo can be found at [sqlbin.vndb.org](https://sqlbin.vndb.org/).

There's no (binary) releases at this point. If you think you have an
interesting use case for this project, let me know and maybe it'll help me get
motivated to turn this into a proper project.
