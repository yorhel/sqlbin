// SPDX-FileCopyrightText: Yoran Heling <projects@yorhel.nl>
// SPDX-License-Identifier: AGPL-3.0-only

// This syntax definition is pretty garbage :(
Prism.languages.gnuplot = {
    'comment': /#.*/,
    'string': [
        { pattern: /"(?:[^\\"\r\n]|\\.)*"/, greedy: true },
        { pattern: /'[^']*'/, greedy: true }
    ],
    // "set-show" options
    'keyword': /\b(?:angles|arrow|autoscale|bind|bmargin|border|boxwidth|boxdepth|chi_shapes|color|colormap|colorsequence|clabel|clip|cntrlabel|cntrparam|colorbox|colornames|contour|cornerpoles|contourfill|dashtype|datafile|decimalsign|dgrid3d|dummy|encoding|errorbars|fit|fontpath|format|grid|hidden3d|history|isosamples|isosurface|isotropic|jitter|key|label|linetype|link|lmargin|loadpath|locale|logscale|macros|mapping|margin|micro|minussign|monochrome|mouse|mttics|multiplot|mx2tics|mxtics|my2tics|mytics|mztics|nonlinear|object|offsets|origin|output|overflow|palette|parametric|paxis|pixmap|pm3d|pointintervalbox|pointsize|polar|print|psdir|raxis|rgbmax|rlabel|rmargin|rrange|rtics|samples|size|spiderplot|style|surface|table|terminal|termoption|theta|tics|ticslevel|ticscale|timestamp|timefmt|title|tmargin|trange|ttics|urange|version|vgrid|view|vrange|vxrange|vyrange|vzrange|walls|watchpoints|x2data|x2dtics|x2label|x2mtics|x2range|x2tics|x2zeroaxis|xdata|xdtics|xlabel|xmtics|xrange|xtics|xyplane|xzeroaxis|y2data|y2dtics|y2label|y2mtics|y2range|y2tics|y2zeroaxis|ydata|ydtics|ylabel|ymtics|yrange|ytics|yzeroaxis|zdata|zdtics|zzeroaxis|cbdata|cbdtics|zero|zeroaxis|zlabel|zmtics|zrange|ztics|cblabel|cbmtics|cbrange|cbtics)\b/,
    // commands
    'symbol': /\b(?:break|cd|call|clear|continue|do|evaluate|exit|fit|help|history|if|for|import|load|local|lower|pause|plot|print|printerr|pwd|quit|raise|refresh|remultiplot|replot|reread|reset|return|save|set|shell|show|splot|stats|system|test|toggle|undefine|unset|update|vclear|vfill|warn|while)\b/,
    // some other syntax keywords
    'function': /\b(?:with|using|axes)\b/,
    'number': /\b(?:\d+(?:\.\d+)?(?:e[+-]?\d+)?)\b/i,
    'punctuation': /[{}[\]|(),.:]/
};

// Based on https://css-tricks.com/creating-an-editable-textarea-that-supports-syntax-highlighted-code/
setHighlight = id => {
    div = document.getElementById(id);
    if (!div) return;
    let textarea = div.querySelector('textarea');
    let pre = document.createElement('pre');
    let code = document.createElement('code');
    code.className = id === 'sqledit' ? 'language-sql' : 'language-gnuplot';
    pre.appendChild(code);
    div.appendChild(pre);

    // For CSS, so styles aren't applied when this JS is disabled.
    div.classList.add('edit-d');
    textarea.classList.add('edit-t');
    pre.classList.add('edit-p');
    code.classList.add('edit-c');

    textarea.oninput = () => {
        let text = textarea.value;
        if(text[text.length-1] == "\n") text += " ";
        code.innerHTML = text.replace(/&/g, '&amp').replace(/</g, '&lt;');
        Prism.highlightElement(code);
        textarea.onscroll();
        textarea.classList.toggle('empty', text === '');
    };
    textarea.onscroll = () => {
        pre.scrollTop = textarea.scrollTop;
        pre.scrollLeft = textarea.scrollLeft;
    };
    textarea.oninput();
    Prism.highlightElement(code);
};

setHighlight('sqledit');
setHighlight('plotedit');
