<!--
SPDX-FileCopyrightText: Yoran Heling <projects@yorhel.nl>
SPDX-License-Identifier: AGPL-3.0-only
-->
This directory contains patches and a build script in order to compile a custom
version of gnuplot. The end result is a stripped-down gnuplot binary that:

- Only supports the "svg" terminal, without mouse support.
- Only accepts scripts on the command-line or standard input, non-interactively.
- Can only write to standard output or error.
- Disables all filesystem I/O, pipes and shell commands.
- Has a few minor changes to the SVG output.

Refer to the patch files for details.

## Requirements

- Linux
- A C build system
- libseccomp
- Perhaps a few other libs I missed

## Usage

- Run `build.sh`
- Point SQLBin to the generated 'gnuplot-sqlbin' binary
