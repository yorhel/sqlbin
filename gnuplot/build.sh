#!/bin/sh
# SPDX-FileCopyrightText: Yoran Heling <projects@yorhel.nl>
# SPDX-License-Identifier: AGPL-3.0-only

GNUPLOT_VERSION=6.0.0
GNUPLOT_DIR=gnuplot-$GNUPLOT_VERSION
GNUPLOT_ARCHIVE=$GNUPLOT_DIR.tar.gz
GNUPLOT_DOWNLOAD=https://downloads.sourceforge.net/project/gnuplot/gnuplot/$GNUPLOT_VERSION/$GNUPLOT_ARCHIVE

SRCPATH=$1

set -ex

rm -rf gnuplot-sqlbin "$GNUPLOT_DIR" gnuplot-src

if [ -n "$SRCPATH" ]
then
    rsync -av "$SRCPATH" gnuplot-src/
    cd gnuplot-src/
    ./prepare
else
    [ -e "$GNUPLOT_ARCHIVE" ] || curl -L "$GNUPLOT_DOWNLOAD" -o "$GNUPLOT_ARCHIVE"
    tar -xzf "$GNUPLOT_ARCHIVE"

    cd "$GNUPLOT_DIR"
    cat ../*.patch | patch -tp1
    autoreconf -fi
fi

./configure --disable-plugins\
    --disable-history-file\
    --disable-wxwidgets\
    --disable-x11-mbfonts\
    --disable-x11-external\
    --without-libcerf\
    --without-cairo\
    --without-gd\
    --without-latex\
    --without-lua\
    --without-qt\
    --without-tektronix\
    --without-x
make -C src timestamp.h # Makefile is missing a dependency
make -C src -j8 gnuplot

cp src/gnuplot ../gnuplot-sqlbin
strip ../gnuplot-sqlbin
