# SPDX-FileCopyrightText: Yoran Heling <projects@yorhel.nl>
# SPDX-License-Identifier: AGPL-3.0-only

.PHONY: release debug clean distclean

PRISM_VER=1.29.0
PRISM_CDN=https://cdnjs.cloudflare.com/ajax/libs/prism/${PRISM_VER}/

BIN_DEPS=main.cr style.css lib/script.js

release: ${BIN_DEPS}
	shards build --release

debug: ${BIN_DEPS}
	shards build --debug

clean:
	rm -rf lib/*.js
	rm -rf bin/

distclean: clean
	rm -rf lib/

lib:
	mkdir -p lib

lib/script.js: script.js lib/prism-${PRISM_VER}.min.js lib/prism-${PRISM_VER}-sql.min.js | lib
	(\
		echo '// @license magnet:?xt=urn:btih:d3d9a9a6595521f9666a5e94cc830dab83b65699&dn=expat.txt Expat';\
		echo '// @source: https://github.com/PrismJS/prism';\
		cat lib/prism-${PRISM_VER}.min.js;\
		echo;\
		cat lib/prism-${PRISM_VER}-sql.min.js;\
		echo;\
		cat script.js;\
		echo '// @license-end';\
	) >$@

lib/prism-${PRISM_VER}.min.js: | lib
	curl ${PRISM_CDN}prism.min.js -so $@

lib/prism-${PRISM_VER}-sql.min.js: | lib
	curl ${PRISM_CDN}components/prism-sql.min.js -so $@
